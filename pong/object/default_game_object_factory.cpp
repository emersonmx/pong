/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/default_game_object_factory.hpp>

#include <BulletCollision/CollisionShapes/btBox2dShape.h>
#include <BulletCollision/CollisionShapes/btBoxShape.h>
#include <BulletCollision/CollisionShapes/btStaticPlaneShape.h>
#include <BulletDynamics/ConstraintSolver/btSliderConstraint.h>
#include <LinearMath/btVector3.h>

#include <OgreMeshManager.h>
#include <OgreSceneManager.h>

#include <pong/application/application_control.hpp>
#include <pong/object/component/bullet_simulation_physics.hpp>
#include <pong/object/component/bullet_trigger_physics.hpp>
#include <pong/object/component/key_input.hpp>
#include <pong/object/component/ogre_basic_graphics.hpp>
#include <pong/object/component/random_initial_input.hpp>

namespace pong {

DefaultGameObjectFactory::DefaultGameObjectFactory()
        : scene_manager_(NULL), dynamic_world_(NULL), keyboard_(NULL),
          window_width_(0.f), window_height_(0.f), pad_1_lower_limit_(0),
          pad_1_upper_limit_(0), pad_2_lower_limit_(0), pad_2_upper_limit_(0),
          x_border_offset_(15.f), center_x_(0.f), center_y_(0.f),
          simulation_(NULL) {}

GameObject* DefaultGameObjectFactory::CreatePlayer1() {
    GameObject* player = new GameObject();

    SetupPlayer1(player);

    Position position = player->position();
    Rotation rotation = player->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
    BulletBasicPhysics* physics = new BulletBasicPhysics();
    physics->set_transform(transform);

    int mask = kBall;
    btRigidBody* rigid_body = CreatePlayer1RigidBody(player, physics);
    dynamic_world_->addRigidBody(rigid_body, kPad, mask);

    physics->set_rigid_body(rigid_body);
    physics->set_world(dynamic_world_);

    btTypedConstraint* constraint = CreatePlayer1Constraint(rigid_body);
    dynamic_world_->addConstraint(constraint, true);

    OIS::KeyCode keys[KeyInput::kCommandSize] = {
        OIS::KC_UP, OIS::KC_DOWN, OIS::KC_LEFT, OIS::KC_RIGHT, OIS::KC_RETURN,
        OIS::KC_ESCAPE, OIS::KC_P, OIS::KC_SPACE
    };
    KeyInput* input = new KeyInput(keyboard_);
    for (int i = 0; i < KeyInput::kCommandSize; i++) {
        input->set_command(static_cast<KeyInput::Command>(i), keys[i]);
    }

    player->set_input(input);
    player->set_physics(physics);
    Ogre::SceneNode* node = scene_manager_->getSceneNode("pad_1");
    GraphicsComponent* graphics = new OgreBasicGraphics(node);
    player->set_graphics(graphics);

    return player;
}

void DefaultGameObjectFactory::SetupPlayer1(GameObject* player) {
    Ogre::MeshManager& mesh_manager = Ogre::MeshManager::getSingleton();
    const Ogre::AxisAlignedBox& wall_rect = static_cast<Ogre::MeshPtr>(
        mesh_manager.getByName("wall.mesh"))->getBounds();

    Ogre::SceneNode* node = scene_manager_->getSceneNode("pad_1");
    node->_update(true, true);

    const Ogre::AxisAlignedBox& pad_rect = node->_getWorldAABB();

    mxgame::Real offset = wall_rect.getSize().y + pad_rect.getHalfSize().y;

    pad_1_lower_limit_ = offset - center_y_;
    pad_1_upper_limit_ = center_y_ - offset;

    Ogre::Vector3 pad_size = pad_rect.getSize();
    Size size = { pad_size.x, pad_size.y, pad_size.z };

    player->set_position(x_border_offset_, center_y_);
    player->set_size(size);
    player->set_movement_velocity(250.f);
}

btRigidBody* DefaultGameObjectFactory::CreatePlayer1RigidBody(
        GameObject* player, BulletBasicPhysics* physics) {

    btScalar mass = 1.f;
    btVector3 inertia(0, 0, 0);

    Size size = player->size();
    btVector3 shape_size(size.x / 2.f, size.y / 2.f, size.z / 2.f);
#ifdef DEBUG
    btCollisionShape* collision_shape = new btBoxShape(shape_size);
#else
    btCollisionShape* collision_shape = new btBox2dShape(shape_size);
#endif
    if (mass > 0.f) {
        collision_shape->calculateLocalInertia(mass, inertia);
    }

    btRigidBody::btRigidBodyConstructionInfo info(mass, physics,
            collision_shape, inertia);
    btRigidBody* rigid_body = new btRigidBody(info);
    rigid_body->setLinearFactor(btVector3(0, 1, 0));
    rigid_body->setAngularFactor(btVector3(0, 0, 0));
    rigid_body->setDamping(0.999f, 1);
    rigid_body->setRestitution(1);
    rigid_body->setFriction(1);
    rigid_body->setActivationState(DISABLE_DEACTIVATION);

    return rigid_body;
}

btTypedConstraint* DefaultGameObjectFactory::CreatePlayer1Constraint(
        btRigidBody* rigid_body) {

    btTransform axis;
    axis.setIdentity();
    axis.setRotation(btQuaternion(0, 0, 1, 1));

    btSliderConstraint* constraint =
        new btSliderConstraint(*rigid_body, axis, true);
    constraint->setLowerLinLimit(pad_1_lower_limit_);
    constraint->setUpperLinLimit(pad_1_upper_limit_);

    return constraint;
}

GameObject* DefaultGameObjectFactory::CreatePlayer2() {
    GameObject* player = new GameObject();

    SetupPlayer2(player);

    Position position = player->position();
    Rotation rotation = player->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
    BulletBasicPhysics* physics = new BulletBasicPhysics();
    physics->set_transform(transform);

    int mask = kBall;
    btRigidBody* rigid_body = CreatePlayer2RigidBody(player, physics);
    dynamic_world_->addRigidBody(rigid_body, kPad, mask);

    physics->set_rigid_body(rigid_body);
    physics->set_world(dynamic_world_);

    btTypedConstraint* constraint = CreatePlayer2Constraint(rigid_body);
    dynamic_world_->addConstraint(constraint, true);

    OIS::KeyCode keys[KeyInput::kCommandSize] = {
        OIS::KC_W, OIS::KC_S, OIS::KC_A, OIS::KC_D, OIS::KC_E, OIS::KC_Q,
        OIS::KC_L, OIS::KC_F
    };
    KeyInput* input = new KeyInput(keyboard_);
    for (int i = 0; i < KeyInput::kCommandSize; i++) {
        input->set_command(static_cast<KeyInput::Command>(i), keys[i]);
    }

    player->set_input(input);
    player->set_physics(physics);
    Ogre::SceneNode* node = scene_manager_->getSceneNode("pad_2");
    GraphicsComponent* graphics = new OgreBasicGraphics(node);
    player->set_graphics(graphics);

    return player;
}

void DefaultGameObjectFactory::SetupPlayer2(GameObject* player) {
    Ogre::MeshManager& mesh_manager = Ogre::MeshManager::getSingleton();
    const Ogre::AxisAlignedBox& wall_rect = static_cast<Ogre::MeshPtr>(
        mesh_manager.getByName("wall.mesh"))->getBounds();

    Ogre::SceneNode* node = scene_manager_->getSceneNode("pad_2");
    node->_update(true, true);

    const Ogre::AxisAlignedBox& pad_rect = node->_getWorldAABB();

    mxgame::Real offset = wall_rect.getSize().y + pad_rect.getHalfSize().y;

    pad_2_lower_limit_ = offset - center_y_;
    pad_2_upper_limit_ = center_y_ - offset;

    Ogre::Vector3 pad_size = pad_rect.getSize();
    Size size = { pad_size.x, pad_size.y, pad_size.z };

    player->set_position(window_width_ - x_border_offset_, center_y_);
    player->set_size(size);
    player->set_movement_velocity(250.f);
}

btRigidBody* DefaultGameObjectFactory::CreatePlayer2RigidBody(
        GameObject* player, BulletBasicPhysics* physics) {

    return CreatePlayer1RigidBody(player, physics);
}

btTypedConstraint* DefaultGameObjectFactory::CreatePlayer2Constraint(
        btRigidBody* rigid_body) {

    btTransform axis;
    axis.setIdentity();
    axis.setRotation(btQuaternion(0, 0, 1, 1));

    btSliderConstraint* constraint =
        new btSliderConstraint(*rigid_body, axis, true);
    constraint->setLowerLinLimit(pad_2_lower_limit_);
    constraint->setUpperLinLimit(pad_2_upper_limit_);

    return constraint;
}

GameObject* DefaultGameObjectFactory::CreateBall() {
    GameObject* ball = new GameObject();

    SetupBall(ball);

    Position position = ball->position();
    Rotation rotation = ball->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));

    BulletSimulationPhysics* physics = new BulletSimulationPhysics();
    physics->set_transform(transform);

    int mask = kPad | kWall | kTrigger;
    btRigidBody* rigid_body = CreateBallRigidBody(ball, physics);
    dynamic_world_->addRigidBody(rigid_body, kBall, mask);

    physics->set_rigid_body(rigid_body);
    physics->set_world(dynamic_world_);
    simulation_->Add(physics);

    InputComponent* random_initial_input = new RandomInitialInput();
    ball->set_input(random_initial_input);
    ball->set_physics(physics);
    Ogre::SceneNode* node = scene_manager_->getSceneNode("ball");
    GraphicsComponent* graphics = new OgreBasicGraphics(node);
    ball->set_graphics(graphics);

    return ball;
}

void DefaultGameObjectFactory::SetupBall(GameObject* ball) {
    Ogre::SceneNode* node = scene_manager_->getSceneNode("ball");
    node->_update(true, true);

    const Ogre::AxisAlignedBox& ball_rect = node->_getWorldAABB();

    Ogre::Vector3 ball_size = ball_rect.getSize();
    Size size = { ball_size.x, ball_size.y, ball_size.z };

    ball->set_position(center_x_, center_y_);
    ball->set_size(size);
    ball->set_movement_velocity(50.f);
    ball->set_rotation_velocity(10.f);
    Clamp clamp = { { 0.f, 500.f }, { 0.f, 10.f } };
    ball->set_clamp(clamp);
}

btRigidBody* DefaultGameObjectFactory::CreateBallRigidBody(GameObject* ball,
        BulletSimulationPhysics* physics) {

    btScalar mass = 1.f;
    btVector3 inertia(0, 0, 0);

    Size size = ball->size();
    btVector3 shape_size(size.x / 2.f, size.y / 2.f, size.z / 2.f);
#ifdef DEBUG
    btCollisionShape* collision_shape = new btBoxShape(shape_size);
#else
    btCollisionShape* collision_shape = new btBox2dShape(shape_size);
#endif
    if (mass > 0.f) {
        collision_shape->calculateLocalInertia(mass, inertia);
    }

    btRigidBody::btRigidBodyConstructionInfo info(mass, physics,
        collision_shape, inertia);
    btRigidBody* rigid_body = new btRigidBody(info);
    rigid_body->setLinearFactor(btVector3(1, 1, 0));
    rigid_body->setAngularFactor(btVector3(0, 0, 1));
    rigid_body->setDamping(0.001, 0.1f);
    rigid_body->setFriction(1);
    rigid_body->setRestitution(1);
    rigid_body->setActivationState(DISABLE_DEACTIVATION);

    return rigid_body;
}

GameObject* DefaultGameObjectFactory::CreateTopWall() {
    GameObject* wall = new GameObject();

    SetupTopWall(wall);

    Position position = wall->position();
    Rotation rotation = wall->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(
        btVector3(position.x, position.y - wall->size().y / 2.f, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));

    BulletBasicPhysics* physics = new BulletBasicPhysics();
    physics->set_transform(transform);

    int mask = kBall;
    btRigidBody* rigid_body = CreateTopWallRigidBody(wall, physics);
    dynamic_world_->addRigidBody(rigid_body, kWall, mask);

    physics->set_rigid_body(rigid_body);
    physics->set_world(dynamic_world_);

    InputComponent* input = new NoInputComponent();
    wall->set_input(input);
    wall->set_physics(physics);
    Ogre::SceneNode* node = scene_manager_->getSceneNode("top_wall");
    GraphicsComponent* graphics = new OgreBasicGraphics(node);
    wall->set_graphics(graphics);

    return wall;
}

void DefaultGameObjectFactory::SetupTopWall(GameObject* wall) {
    Ogre::SceneNode* node = scene_manager_->getSceneNode("top_wall");
    node->_update(true, true);

    const Ogre::AxisAlignedBox& wall_rect = node->_getWorldAABB();

    Ogre::Vector3 wall_size = wall_rect.getSize();
    Size size = { wall_size.x, wall_size.y, wall_size.z };

    wall->set_position(center_x_, window_height_ - size.y / 2.f);
    wall->set_size(size);
}

btRigidBody* DefaultGameObjectFactory::CreateTopWallRigidBody(GameObject* wall,
        BulletBasicPhysics* physics) {

    btVector3 normal(0, -1, 0);
    btScalar mass = 0.f;
    btVector3 inertia(0, 0, 0);

    btCollisionShape* collision_shape = new btStaticPlaneShape(normal, 0);

    btRigidBody::btRigidBodyConstructionInfo info(mass, physics,
            collision_shape, inertia);
    btRigidBody* rigid_body = new btRigidBody(info);
    rigid_body->setLinearFactor(btVector3(1, 1, 0));
    rigid_body->setAngularFactor(btVector3(0, 0, 1));
    rigid_body->setRestitution(1);
    rigid_body->setFriction(1);

    return rigid_body;
}

GameObject* DefaultGameObjectFactory::CreateBottomWall() {
    GameObject* wall = new GameObject();

    SetupBottomWall(wall);

    Position position = wall->position();
    Rotation rotation = wall->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(
        btVector3(position.x, position.y + wall->size().y / 2.f, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));

    BulletBasicPhysics* physics = new BulletBasicPhysics();
    physics->set_transform(transform);

    int mask = kBall;
    btRigidBody* rigid_body = CreateBottomWallRigidBody(wall, physics);
    dynamic_world_->addRigidBody(rigid_body, kWall, mask);

    physics->set_rigid_body(rigid_body);
    physics->set_world(dynamic_world_);

    InputComponent* input = new NoInputComponent();
    wall->set_input(input);
    wall->set_physics(physics);
    Ogre::SceneNode* node = scene_manager_->getSceneNode("bottom_wall");
    GraphicsComponent* graphics = new OgreBasicGraphics(node);
    wall->set_graphics(graphics);

    return wall;
}

void DefaultGameObjectFactory::SetupBottomWall(GameObject* wall) {
    Ogre::SceneNode* node = scene_manager_->getSceneNode("bottom_wall");
    node->_update(true, true);

    const Ogre::AxisAlignedBox& wall_rect = node->_getWorldAABB();

    Ogre::Vector3 wall_size = wall_rect.getSize();
    Size size = { wall_size.x , wall_size.y, wall_size.z };

    wall->set_position(center_x_, size.y / 2.f);
    wall->set_size(size);
}

btRigidBody* DefaultGameObjectFactory::CreateBottomWallRigidBody(
        GameObject* wall, BulletBasicPhysics* physics) {

    btVector3 normal(0, 1, 0);
    btScalar mass = 0.f;
    btVector3 inertia(0, 0, 0);

    btCollisionShape* collision_shape = new btStaticPlaneShape(normal, 0);

    btRigidBody::btRigidBodyConstructionInfo info(mass, physics,
            collision_shape, inertia);
    btRigidBody* rigid_body = new btRigidBody(info);
    rigid_body->setLinearFactor(btVector3(1, 1, 0));
    rigid_body->setAngularFactor(btVector3(0, 0, 1));
    rigid_body->setRestitution(1);
    rigid_body->setFriction(1);

    return rigid_body;
}

GameObject* DefaultGameObjectFactory::CreateTriggerPlayer1() {
    GameObject* trigger = new GameObject();

    SetupTriggerPlayer1(trigger);

    Position position = trigger->position();
    Rotation rotation = trigger->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
    BulletTriggerPhysics* physics = new BulletTriggerPhysics();
    physics->set_transform(transform);

    int mask = kBall;
    btGhostObject* ghost = CreateTriggerPlayer1GhostObject(trigger, physics);
    dynamic_world_->addCollisionObject(ghost, kTrigger, mask);

    physics->set_ghost(ghost);
    physics->set_world(dynamic_world_);
    physics->set_application_control(application_control_);
    simulation_->Add(physics);

    trigger->set_input(new NoInputComponent());
    trigger->set_physics(physics);
    trigger->set_graphics(new NoGraphicsComponent());

    return trigger;
}

void DefaultGameObjectFactory::SetupTriggerPlayer1(GameObject* trigger) {
    trigger->set_position(-60, center_y_, 0.f);
    trigger->set_size(100, window_height_, 100);
}

btGhostObject*
DefaultGameObjectFactory::CreateTriggerPlayer1GhostObject(
        GameObject* trigger, BulletTriggerPhysics* physics) {

    Size size = trigger->size();
    btVector3 shape_size(size.x / 2.f, size.y / 2.f, size.z / 2.f);
#ifdef DEBUG
    btCollisionShape* collision_shape = new btBoxShape(shape_size);
#else
    btCollisionShape* collision_shape = new btBox2dShape(shape_size);
#endif

    btGhostObject* ghost = new btGhostObject();
    ghost->setWorldTransform(physics->transform());
    ghost->setCollisionShape(collision_shape);
    ghost->setActivationState(DISABLE_DEACTIVATION);
    ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);

    return ghost;
}

GameObject* DefaultGameObjectFactory::CreateTriggerPlayer2() {
    GameObject* trigger = new GameObject();

    SetupTriggerPlayer2(trigger);

    Position position = trigger->position();
    Rotation rotation = trigger->rotation();

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    transform.setRotation(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
    BulletTriggerPhysics* physics = new BulletTriggerPhysics();
    physics->set_transform(transform);

    int mask = kBall;
    btGhostObject* ghost = CreateTriggerPlayer2GhostObject(trigger, physics);
    dynamic_world_->addCollisionObject(ghost, kTrigger, mask);

    physics->set_ghost(ghost);
    physics->set_world(dynamic_world_);
    physics->set_application_control(application_control_);
    simulation_->Add(physics);

    trigger->set_input(new NoInputComponent());
    trigger->set_physics(physics);
    trigger->set_graphics(new NoGraphicsComponent());

    return trigger;
}

void DefaultGameObjectFactory::SetupTriggerPlayer2(GameObject* trigger) {
    trigger->set_position(window_width_ + 60, center_y_, 0.f);
    trigger->set_size(100, window_height_, 100);
}

btGhostObject* DefaultGameObjectFactory::CreateTriggerPlayer2GhostObject(
        GameObject* trigger, BulletTriggerPhysics* physics) {

    return CreateTriggerPlayer1GhostObject(trigger, physics);
}

} /* namespace pong */

