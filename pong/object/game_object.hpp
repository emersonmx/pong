/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_GAME_OBJECT_HPP_
#define PONG_OBJECT_GAME_OBJECT_HPP_

#include <mxgame/core/types.hpp>

#include <pong/object/component/component.hpp>

namespace pong {

class GameObject {
    public:
        GameObject(InputComponent* input=NULL, PhysicsComponent* physics=NULL,
                   GraphicsComponent* graphics=NULL);

        virtual ~GameObject() {}

        inline Position position() const { return position_; }

        void set_position(mxgame::Real x=0.f, mxgame::Real y=0.f,
                          mxgame::Real z=0.f);

        inline void set_position(const Position& position) {
            set_position(position.x, position.y, position.z);
        }

        inline Rotation rotation() const { return rotation_; }

        void set_rotation(mxgame::Real w=1.f, mxgame::Real x=0.f,
                          mxgame::Real y=0.f, mxgame::Real z=0.f);

        inline void set_rotation(const Rotation& rotation) {
            set_rotation(rotation.w, rotation.x, rotation.y, rotation.z);
        }

        inline Size size() const { return size_; }

        void set_size(mxgame::Real x=1.f, mxgame::Real y=1.f,
                      mxgame::Real z=1.f);

        inline void set_size(const Size& size) {
            set_size(size.x, size.y, size.z);
        }

        inline LinearVelocity linear_velocity() const {
            return linear_velocity_;
        }

        void set_linear_velocity(mxgame::Real x=0.f, mxgame::Real y=0.f,
                                 mxgame::Real z=0.f);

        inline void set_linear_velocity(const LinearVelocity& linear_velocity) {
            set_linear_velocity(linear_velocity.x, linear_velocity.y,
                                linear_velocity.z);
        }

        inline AngularVelocity angular_velocity() const {
            return angular_velocity_;
        }

        void set_angular_velocity(mxgame::Real x=0.f, mxgame::Real y=0.f,
                                  mxgame::Real z=0.f);

        inline void
        set_angular_velocity(const AngularVelocity& angular_velocity) {
            set_angular_velocity(angular_velocity.x, angular_velocity.y,
                                 angular_velocity.z);
        }

        inline Clamp clamp() const { return clamp_; }

        inline void set_clamp(const Clamp& clamp) { clamp_ = clamp; }

        inline mxgame::Real movement_velocity() const {
            return movement_velocity_;
        }

        inline void set_movement_velocity(mxgame::Real movement_velocity=0.f) {
            movement_velocity_ = movement_velocity;
        }

        inline mxgame::Real rotation_velocity() const {
            return rotation_velocity_;
        }

        inline void set_rotation_velocity(mxgame::Real rotation_velocity=0.f) {
            rotation_velocity_ = rotation_velocity;
        }

        inline InputComponent* input() const { return input_; }

        inline void set_input(InputComponent* input) { input_ = input; }

        inline PhysicsComponent* physics() const { return physics_; }

        inline void set_physics(PhysicsComponent* physics) {
            physics_ = physics;
        }

        inline GraphicsComponent* graphics() const { return graphics_; }

        inline void set_graphics(GraphicsComponent* graphics) {
            graphics_ = graphics;
        }

        void NotifyChanges(Component* to, Message message) {
            to->Changes(this, message);
        }

        void Setup();

        void Update();

        void Destroy();

    private:
        void Reset();

        Position position_;
        Rotation rotation_;
        Size size_;

        LinearVelocity linear_velocity_;
        AngularVelocity angular_velocity_;

        Clamp clamp_;

        mxgame::Real movement_velocity_;
        mxgame::Real rotation_velocity_;

        InputComponent* input_;
        PhysicsComponent* physics_;
        GraphicsComponent* graphics_;
};

} /* namespace pong */
#endif /* PONG_OBJECT_GAME_OBJECT_HPP_ */

