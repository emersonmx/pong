/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_GAME_OBJECT_FACTORY_HPP_
#define PONG_OBJECT_GAME_OBJECT_FACTORY_HPP_

namespace pong {

class GameObject;

class GameObjectFactory {
    public:
        virtual ~GameObjectFactory() {}

        virtual GameObject* CreatePlayer1() = 0;

        virtual GameObject* CreatePlayer2() = 0;

        virtual GameObject* CreateBall() = 0;

        virtual GameObject* CreateTopWall() = 0;

        virtual GameObject* CreateBottomWall() = 0;

        virtual GameObject* CreateTriggerPlayer1() = 0;

        virtual GameObject* CreateTriggerPlayer2() = 0;
};

} /* namespace pong */
#endif /* PONG_OBJECT_GAME_OBJECT_FACTORY_HPP_ */

