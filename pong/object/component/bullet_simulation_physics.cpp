/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/component/bullet_simulation_physics.hpp>

#include <pong/object/game_object.hpp>

namespace pong {

void BulletSimulationPhysics::Tick(btDynamicsWorld* world, btScalar time_step) {
    if (world == world_) {
        const int kTimeBoost = 3;
        btScalar hurry_movement = time_step * kTimeBoost;
        game_object_->set_movement_velocity(
            game_object_->movement_velocity() + hurry_movement);

        if (game_object_->movement_velocity() >=
                game_object_->clamp().linear_velocity.maximum) {

            game_object_->set_movement_velocity(
                game_object_->clamp().linear_velocity.maximum - 1);
        }

        btVector3 velocity = rigid_body_->getLinearVelocity();
        btScalar speed = velocity.length();
        if (speed < game_object_->movement_velocity()) {
            velocity *= game_object_->movement_velocity() / speed;
        } else if (speed > game_object_->clamp().linear_velocity.maximum) {
            velocity *= game_object_->clamp().linear_velocity.maximum / speed;
        }

        rigid_body_->setLinearVelocity(velocity);

        velocity = rigid_body_->getAngularVelocity();
        speed = velocity.length();
        if (speed < game_object_->clamp().angular_velocity.minimum) {
            velocity *= game_object_->clamp().angular_velocity.minimum / speed;
        } else if (speed > game_object_->clamp().angular_velocity.maximum) {
            velocity *= game_object_->clamp().angular_velocity.maximum / speed;
        }

        rigid_body_->setAngularVelocity(velocity);
        game_object_->set_rotation_velocity(speed);
    }
}

} /* namespace pong */

