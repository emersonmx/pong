/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pont.

  pont is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pont is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pont.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/component/key_input.hpp>

#include <pong/object/game_object.hpp>

namespace pong {

KeyInput::KeyInput(OIS::Keyboard* keyboard) : keyboard_(keyboard) {
    for (int i = 0; i < kCommandSize; i++) {
        gamepad_[i] = OIS::KC_UNASSIGNED;
    }
}

void KeyInput::Update(GameObject* game_object) {
    if (keyboard_->isKeyDown(gamepad_[kUp])) {
        game_object->set_linear_velocity(0, game_object->linear_velocity().y +
            game_object->movement_velocity());

        game_object->NotifyChanges(game_object->physics(), kLinearVelocity);
    } else if (keyboard_->isKeyDown(gamepad_[kDown])) {
        game_object->set_linear_velocity(0, game_object->linear_velocity().y -
            game_object->movement_velocity());

        game_object->NotifyChanges(game_object->physics(), kLinearVelocity);
    }
}

} /* namespace pong */

