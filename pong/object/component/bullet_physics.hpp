/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_COMPONENT_BULLET_PHYSICS_HPP_
#define PONG_OBJECT_COMPONENT_BULLET_PHYSICS_HPP_

#include <LinearMath/btMotionState.h>

#include <mxgame/physics/bullet_simulation_listener.hpp>

#include <pong/object/component/component.hpp>

namespace pong {

class BulletPhysics
        : public DefaultPhysicsComponent, public btMotionState,
          public mxgame::BulletSimulationListener {

    public:
        BulletPhysics()
                : transform_(btTransform::getIdentity()), world_(NULL),
                  game_object_(NULL) {}

        inline btTransform transform() const { return transform_; }

        inline void set_transform(const btTransform& transform) {
            transform_ = transform;
        }

        inline btDynamicsWorld* world() const { return world_; }

        inline void set_world(btDynamicsWorld* world) { world_ = world; }

        virtual void getWorldTransform(btTransform& world_transform) const {
            world_transform = transform_;
        }

        virtual void setWorldTransform(const btTransform& world_transform) {
            transform_ = world_transform;
        }

        virtual void Setup(GameObject* game_object) {
            game_object_ = game_object;
        }

    protected:
        virtual void Tick(btDynamicsWorld* world, btScalar time_step) {}

        btTransform transform_;

        btDynamicsWorld* world_;

        GameObject* game_object_;
};

} /* namespace pong */
#endif /* PONG_OBJECT_COMPONENT_BULLET_PHYSICS_HPP_ */

