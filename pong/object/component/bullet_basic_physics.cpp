/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/component/bullet_basic_physics.hpp>

#include <pong/object/game_object.hpp>

namespace pong {

BulletBasicPhysics::BulletBasicPhysics()
        : BulletPhysics(), rigid_body_(NULL) {}

void BulletBasicPhysics::setWorldTransform(const btTransform& world_transform) {
    BulletPhysics::setWorldTransform(world_transform);

    btVector3 vector3 = transform_.getOrigin();
    btQuaternion quaternion = transform_.getRotation();

    game_object_->set_position(vector3.x(), vector3.y(), vector3.z());
    game_object_->set_rotation(quaternion.w(), quaternion.x(), quaternion.y(),
                               quaternion.z());

    game_object_->NotifyChanges(game_object_->graphics(),
        static_cast<Message>(kPosition | kRotation));
}

void BulletBasicPhysics::Changes(GameObject* game_object, Message message) {
    if ((message & kLinearVelocity) == kLinearVelocity) {
        LinearVelocity linear_velocity = game_object->linear_velocity();
        btVector3 velocity(linear_velocity.x, linear_velocity.y,
                           linear_velocity.z);
        rigid_body_->setLinearVelocity(velocity);
    }

    if ((message & kAngularVelocity) == kAngularVelocity) {
        AngularVelocity angular_velocity = game_object->angular_velocity();

        btVector3 velocity(angular_velocity.x, angular_velocity.y,
                           angular_velocity.z);

        rigid_body_->setAngularVelocity(velocity);
    }
}

} /* namespace pong */

