/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_COMPONENT_BULLET_TRIGGER_PHYSICS_HPP_
#define PONG_OBJECT_COMPONENT_BULLET_TRIGGER_PHYSICS_HPP_

#include <BulletCollision/CollisionDispatch/btGhostObject.h>

#include <pong/object/component/bullet_physics.hpp>

namespace pong {

class ApplicationControl;

class BulletTriggerPhysics : public BulletPhysics {
    public:
        BulletTriggerPhysics();

        inline btGhostObject* ghost() const { return ghost_; }

        inline void set_ghost(btGhostObject* ghost) {
            ghost_ = ghost;
        }

        inline ApplicationControl* application_control() const {
            return application_control_;
        }

        inline void set_application_control(
                ApplicationControl* application_control) {

            application_control_ = application_control;
        }

    protected:
        virtual void Tick(btDynamicsWorld* world, btScalar time_step);

        btGhostObject* ghost_;

        ApplicationControl* application_control_;
};

} /* namespace pong */
#endif /* PONG_OBJECT_COMPONENT_BULLET_TRIGGER_PHYSICS_HPP_ */

