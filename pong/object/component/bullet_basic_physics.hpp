/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_COMPONENT_BULLET_BASIC_PHYSICS_HPP_
#define PONG_OBJECT_COMPONENT_BULLET_BASIC_PHYSICS_HPP_

#include <BulletDynamics/Dynamics/btDynamicsWorld.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>

#include <pong/object/component/bullet_physics.hpp>

namespace pong {

class BulletBasicPhysics : public BulletPhysics {
    public:
        BulletBasicPhysics();

        inline btRigidBody* rigid_body() const { return rigid_body_; }

        inline void set_rigid_body(btRigidBody* rigid_body) {
            rigid_body_ = rigid_body;
        }

        virtual void setWorldTransform(const btTransform& world_transform);

        virtual void Changes(GameObject* game_object, Message message);

    protected:
        btRigidBody* rigid_body_;
};

} /* namespace pong */
#endif /* PONG_OBJECT_COMPONENT_BULLET_BASIC_PHYSICS_HPP_ */

