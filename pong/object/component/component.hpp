/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_COMPONENT_COMPONENT_HPP_
#define PONG_OBJECT_COMPONENT_COMPONENT_HPP_

#include <pong/core/types.hpp>

namespace pong {

class GameObject;

class Component {
    public:
        virtual ~Component() {}

        virtual void Changes(GameObject* game_object, Message message) = 0;

        virtual void Setup(GameObject* game_object) = 0;

        virtual void Update(GameObject* game_object) = 0;

        virtual void Destroy(GameObject* game_object) = 0;
};

class NoComponent : public Component {
    public:
        virtual void Changes(GameObject* game_object, Message message) {}

        virtual void Setup(GameObject* game_object) {}

        virtual void Update(GameObject* game_object) {}

        virtual void Destroy(GameObject* game_object) {}
};

class InputComponent : public Component {};

class DefaultInputComponent : public InputComponent {
    public:
        virtual void Changes(GameObject* game_object, Message message) {}

        virtual void Setup(GameObject* game_object) {}

        virtual void Update(GameObject* game_object) {}

        virtual void Destroy(GameObject* game_object) {}
};

class NoInputComponent : public DefaultInputComponent {};

class PhysicsComponent : public Component {};

class DefaultPhysicsComponent : public PhysicsComponent {
    public:
        virtual void Changes(GameObject* game_object, Message message) {}

        virtual void Setup(GameObject* game_object) {}

        virtual void Update(GameObject* game_object) {}

        virtual void Destroy(GameObject* game_object) {}
};

class NoPhysicsComponent : public DefaultPhysicsComponent {};

class GraphicsComponent : public Component {};

class DefaultGraphicsComponent : public GraphicsComponent {
    public:
        virtual void Changes(GameObject* game_object, Message message) {}

        virtual void Setup(GameObject* game_object) {}

        virtual void Update(GameObject* game_object) {}

        virtual void Destroy(GameObject* game_object) {}
};

class NoGraphicsComponent : public DefaultGraphicsComponent {};

} /* namespace pong */
#endif /* PONG_OBJECT_COMPONENT_COMPONENT_HPP_ */

