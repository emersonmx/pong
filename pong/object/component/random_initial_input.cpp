/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/component/random_initial_input.hpp>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include <pong/object/game_object.hpp>

namespace pong {

void RandomInitialInput::Setup(GameObject* game_object) {
    mxgame::Real rotation_velocity = game_object->rotation_velocity();

    boost::random::mt19937 generator(time(0));
    boost::random::uniform_real_distribution<> distribution_f(
        -rotation_velocity, rotation_velocity);
    boost::random::uniform_real_distribution<> distribution_1f(-1.f, 1.f);
    boost::random::uniform_int_distribution<> distribution_1(0, 1);

    mxgame::Real way = distribution_1(generator) ? 1.f : -1.f;
    mxgame::Real angular_velocity = distribution_f(generator);

    game_object->set_linear_velocity(way, distribution_1f(generator));
    game_object->set_angular_velocity(0, 0, angular_velocity);

    game_object->NotifyChanges(game_object->physics(),
        static_cast<Message>(kLinearVelocity | kAngularVelocity));
}

} /* namespace pong */

