/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_COMPONENT_OGRE_BASIC_GRAPHICS_HPP_
#define PONG_OBJECT_COMPONENT_OGRE_BASIC_GRAPHICS_HPP_

#include <cstddef>

#include <pong/object/component/component.hpp>

namespace Ogre {

class SceneNode;

} /* namespace Ogre */

namespace pong {

class OgreBasicGraphics : public DefaultGraphicsComponent {
    public:
        OgreBasicGraphics(Ogre::SceneNode* scene_node=NULL)
                : scene_node_(scene_node) {}

        inline Ogre::SceneNode* scene_node() const { return scene_node_; }

        inline void set_scene_node(Ogre::SceneNode* scene_node) {
            scene_node_ = scene_node;
        }

        virtual void Changes(GameObject* game_object, Message message);

        virtual void Setup(GameObject* game_object);

    private:
        Ogre::SceneNode* scene_node_;
};

} /* namespace pong */
#endif /* PONG_OBJECT_COMPONENT_OGRE_BASIC_GRAPHICS_HPP_ */
