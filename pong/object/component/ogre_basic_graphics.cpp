/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/component/ogre_basic_graphics.hpp>

#include <OgreSceneNode.h>

#include <pong/object/game_object.hpp>

namespace pong {

void OgreBasicGraphics::Changes(GameObject* game_object, Message message) {
    if ((message & kPosition) == kPosition) {
        Position position = game_object->position();

        scene_node_->setPosition(position.x, position.y, position.z);
    }

    if ((message & kRotation) == kRotation) {
        Rotation rotation = game_object->rotation();

        scene_node_->setOrientation(rotation.w, rotation.x, rotation.y, rotation.z);
    }
}

void OgreBasicGraphics::Setup(GameObject* game_object) {
    Position position = game_object->position();
    Rotation rotation = game_object->rotation();

    scene_node_->setPosition(position.x, position.y, position.z);
    scene_node_->setOrientation(rotation.w, rotation.x, rotation.y, rotation.z);
}

} /* namespace pong */

