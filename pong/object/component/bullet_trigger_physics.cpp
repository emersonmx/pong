/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/component/bullet_trigger_physics.hpp>

#include <pong/application/application_control.hpp>
#include <pong/object/game_object.hpp>

namespace pong {

BulletTriggerPhysics::BulletTriggerPhysics() : ghost_(NULL) {}

void BulletTriggerPhysics::Tick(btDynamicsWorld* world, btScalar time_step) {
    if (world_ == world) {
        if (ghost_->getNumOverlappingObjects()) {
            application_control_->Restart();
        }
    }
}

} /* namespace pong */

