/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_COMPONENT_RANDON_INITIAL_INPUT_HPP_
#define PONG_OBJECT_COMPONENT_RANDON_INITIAL_INPUT_HPP_

#include <pong/object/component/component.hpp>

namespace pong {

class RandomInitialInput : public DefaultInputComponent {
    public:
        virtual void Setup(GameObject* game_object);
};

} /* namespace pong */
#endif /* PONG_OBJECT_COMPONENT_RANDON_INITIAL_INPUT_HPP_ */

