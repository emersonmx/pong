/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/object/game_object.hpp>

namespace pong {

GameObject::GameObject(InputComponent* input, PhysicsComponent* physics,
                       GraphicsComponent* graphics)
        : input_(input), physics_(physics), graphics_(graphics) {

    Reset();
}

void GameObject::set_position(mxgame::Real x, mxgame::Real y, mxgame::Real z) {
    position_.x = x;
    position_.y = y;
    position_.z = z;
}

void GameObject::set_rotation(mxgame::Real w, mxgame::Real x, mxgame::Real y,
                              mxgame::Real z) {

    rotation_.w = w;
    rotation_.x = x;
    rotation_.y = y;
    rotation_.z = z;
}

void GameObject::set_size(mxgame::Real x, mxgame::Real y, mxgame::Real z) {
    size_.x = x;
    size_.y = y;
    size_.z = z;
}

void GameObject::set_linear_velocity(mxgame::Real x, mxgame::Real y, mxgame::Real z) {
    linear_velocity_.x = x;
    linear_velocity_.y = y;
    linear_velocity_.z = z;
}

void GameObject::set_angular_velocity(mxgame::Real x, mxgame::Real y, mxgame::Real z) {
    angular_velocity_.x = x;
    angular_velocity_.y = y;
    angular_velocity_.z = z;
}

void GameObject::Setup() {
    input_->Setup(this);
    physics_->Setup(this);
    graphics_->Setup(this);
}

void GameObject::Update() {
    input_->Update(this);
    physics_->Update(this);
    graphics_->Update(this);

    set_linear_velocity();
    set_angular_velocity();
}

void GameObject::Destroy() {
    input_->Destroy(this);
    physics_->Destroy(this);
    graphics_->Destroy(this);
}

void GameObject::Reset() {
    Clamp clamp = { { 0.f, 0.f }, { 0.f, 0.f } };

    set_position();
    set_rotation();
    set_size();
    set_linear_velocity();
    set_angular_velocity();
    set_clamp(clamp);
    set_movement_velocity();
    set_rotation_velocity();
}

} /* namespace pong */

