/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_OBJECT_DEFAULT_GAME_OBJECT_FACTORY_HPP_
#define PONG_OBJECT_DEFAULT_GAME_OBJECT_FACTORY_HPP_

#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletDynamics/Dynamics/btDynamicsWorld.h>

#include <mxgame/physics/bullet_simulation.hpp>

#include <pong/object/game_object.hpp>
#include <pong/object/game_object_factory.hpp>

namespace Ogre {

class SceneManager;

} /* namespace Ogre */

namespace OIS {

class Keyboard;

} /* namespace OIS */

namespace pong {

class ApplicationControl;
class BulletBasicPhysics;
class BulletPhysics;
class BulletSimulationPhysics;
class BulletTriggerPhysics;

class DefaultGameObjectFactory : public GameObjectFactory {
    public:
        typedef enum {
            kNothing,
            kBall = 1,
            kPad = 2,
            kWall = 4,
            kTrigger = 8,
            kAll = -1
        } CollisionType;

        DefaultGameObjectFactory();

        inline Ogre::SceneManager* scene_manager() {
            return scene_manager_;
        }

        inline void set_scene_manager(Ogre::SceneManager* scene_manager) {
            scene_manager_ = scene_manager;
        }

        inline btDynamicsWorld* dynamic_world() { return dynamic_world_; }

        inline void set_dynamic_world(btDynamicsWorld* dynamic_world) {
            dynamic_world_ = dynamic_world;
        }

        inline OIS::Keyboard* keyboard() const { return keyboard_; }

        inline void set_keyboard(OIS::Keyboard* keyboard) {
            keyboard_ = keyboard;
        }

        inline ApplicationControl* application_control() const {
            return application_control_;
        }

        inline void set_application_control(
                ApplicationControl* application_control) {

            application_control_ = application_control;
        }

        inline unsigned window_width() const { return window_width_; }

        inline void set_window_width(unsigned window_width) {
            window_width_ = window_width;
            center_x_ = window_width_ / 2.f;
        }

        inline unsigned window_height() const { return window_height_; }

        inline void set_window_height(unsigned window_height) {
            window_height_ = window_height;
            center_y_ = window_height_ / 2.f;
        }

        inline mxgame::BulletSimulation* simulation() const {
            return simulation_;
        }

        inline void set_simulation(mxgame::BulletSimulation* simulation) {
            simulation_ = simulation;
        }

        virtual GameObject* CreatePlayer1();

        virtual void SetupPlayer1(GameObject* player);

        virtual btRigidBody* CreatePlayer1RigidBody(GameObject* player,
            BulletBasicPhysics* physics);

        virtual btTypedConstraint* CreatePlayer1Constraint(
            btRigidBody* rigid_body);

        virtual GameObject* CreatePlayer2();

        virtual void SetupPlayer2(GameObject* player);

        virtual btRigidBody* CreatePlayer2RigidBody(GameObject* player,
            BulletBasicPhysics* physics);

        virtual btTypedConstraint* CreatePlayer2Constraint(
            btRigidBody* rigid_body);

        virtual GameObject* CreateBall();

        virtual void SetupBall(GameObject* ball);

        virtual btRigidBody* CreateBallRigidBody(GameObject* ball,
            BulletSimulationPhysics* physics);

        virtual GameObject* CreateTopWall();

        virtual void SetupTopWall(GameObject* wall);

        virtual btRigidBody* CreateTopWallRigidBody(GameObject* wall,
            BulletBasicPhysics* physics);

        virtual GameObject* CreateBottomWall();

        virtual void SetupBottomWall(GameObject* wall);

        virtual btRigidBody* CreateBottomWallRigidBody(GameObject* wall,
            BulletBasicPhysics* physics);

        virtual GameObject* CreateTriggerPlayer1();

        virtual void SetupTriggerPlayer1(GameObject* trigger);

        virtual btGhostObject* CreateTriggerPlayer1GhostObject(
            GameObject* trigger, BulletTriggerPhysics* physics);

        virtual GameObject* CreateTriggerPlayer2();

        virtual void SetupTriggerPlayer2(GameObject* trigger);

        virtual btGhostObject* CreateTriggerPlayer2GhostObject(
            GameObject* trigger, BulletTriggerPhysics* physics);

    protected:
        Ogre::SceneManager* scene_manager_;

        btDynamicsWorld* dynamic_world_;

        OIS::Keyboard* keyboard_;

        ApplicationControl* application_control_;

        unsigned window_width_;
        unsigned window_height_;

        int pad_1_lower_limit_;
        int pad_1_upper_limit_;
        int pad_2_lower_limit_;
        int pad_2_upper_limit_;

        mxgame::Real x_border_offset_;
        mxgame::Real center_x_;
        mxgame::Real center_y_;

        mxgame::BulletSimulation* simulation_;
};

} /* namespace pong */
#endif /* PONG_OBJECT_DEFAULT_GAME_OBJECT_FACTORY_HPP_ */

