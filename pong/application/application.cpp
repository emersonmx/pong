/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/application/application.hpp>

#include <OgreEntity.h>
#include <OgreSceneManager.h>

#include <mxgame/util/util.hpp>

#include <pong/object/default_game_object_factory.hpp>

#include <cstdio>

namespace pong {

Application::Application()
        : ball_(NULL), game_object_factory_(NULL) {

    for (int i = 0; i < kPlayerSize; i++) {
        players_[i] = NULL;
    }

    for (int i = 0; i < kWallSize; i++) {
        walls_[i] = NULL;
    }

    for (int i = 0; i < kTriggerSize; i++) {
        triggers_[i] = NULL;
    }
}

void Application::Setup() {
    SetupOgreSceneNodes();
    SetupGameObjects();
}

void Application::SetupOgreSceneNodes() {
    enum { kPadNode, kWallNode, kMidfieldNode, kBallNode, kNodeSize };
    Ogre::String names[] = { "pad", "wall", "midfield", "ball" };
    Ogre::String prefixes[] = { "top", "bottom" };
    Ogre::String meshes[] = {
        "pad.mesh", "wall.mesh", "midfield.mesh", "ball.mesh"
    };

    for (int type = kPadNode; type < kNodeSize; type++) {
        switch (type) {
            case kPadNode:
                for (int i = 0; i < 2; i++) {
                    Ogre::String name =
                        names[type] + "_" + mxgame::ToString(i + 1);
                    Ogre::Entity* entity = scene_manager_->createEntity(
                        name, meshes[type]);
                    Ogre::SceneNode* scene_node =
                        scene_manager_->getRootSceneNode()->
                            createChildSceneNode(name);
                    scene_node->attachObject(entity);
                }
                break;
            case kWallNode:
                for (int i = 0; i < 2; i++) {
                    Ogre::String name = prefixes[i] + "_" + names[type];
                    Ogre::Entity* entity = scene_manager_->createEntity(
                        name, meshes[type]);
                    Ogre::SceneNode* scene_node =
                        scene_manager_->getRootSceneNode()->
                            createChildSceneNode(name);
                    scene_node->attachObject(entity);
                }
                break;
            default:
                Ogre::String name = names[type];
                Ogre::Entity* entity = scene_manager_->createEntity(
                    name, meshes[type]);
                Ogre::SceneNode* scene_node =
                    scene_manager_->getRootSceneNode()->
                        createChildSceneNode(name);
                scene_node->attachObject(entity);

                if (type == kMidfieldNode) {
                    scene_node->setPosition(window_width_ / 2.f,
                        window_height_ / 2.f, 0);
                }
        }
    }
}

void Application::SetupGameObjects() {
    DefaultGameObjectFactory* factory = new DefaultGameObjectFactory();
    factory->set_scene_manager(scene_manager_);
    factory->set_dynamic_world(dynamic_world_);
    factory->set_keyboard(keyboard_);
    factory->set_window_width(window_width_);
    factory->set_window_height(window_height_);
    factory->set_simulation(&simulation_);
    factory->set_application_control(this);
    game_object_factory_ = factory;

    SetupPlayers();
    SetupBall();
    SetupWalls();
    SetupTriggers();

    SetupAllGameObjects();
}

void Application::SetupPlayers() {
    GameObject* player = NULL;
    for (int i = 0; i < kPlayerSize; i++) {
        if (i == kPlayer1) {
            player = game_object_factory_->CreatePlayer1();
        } else {
            player = game_object_factory_->CreatePlayer2();
        }

        components_.push_back(player->input());
        components_.push_back(player->physics());
        components_.push_back(player->graphics());
        players_[i] = player;
    }
}

void Application::SetupBall() {
    ball_ = game_object_factory_->CreateBall();
    components_.push_back(ball_->input());
    components_.push_back(ball_->physics());
    components_.push_back(ball_->graphics());
}

void Application::SetupWalls() {
    GameObject* wall = NULL;
    for (int i = 0; i < kWallSize; i++) {
        if (i == 0) {
            wall = game_object_factory_->CreateTopWall();
        } else {
            wall = game_object_factory_->CreateBottomWall();
        }

        components_.push_back(wall->input());
        components_.push_back(wall->physics());
        components_.push_back(wall->graphics());
        walls_[i] = wall;
    }
}

void Application::SetupTriggers() {
    GameObject* trigger = NULL;
    for (int i = 0; i < kTriggerSize; i++) {
        if (i == kTriggerPlayer1) {
            trigger = game_object_factory_->CreateTriggerPlayer1();
        } else {
            trigger = game_object_factory_->CreateTriggerPlayer2();
        }

        components_.push_back(trigger->input());
        components_.push_back(trigger->physics());
        components_.push_back(trigger->graphics());
        triggers_[i] = trigger;
    }
}

void Application::SetupAllGameObjects() {
    for (int i = 0; i < kPlayerSize; i++) {
        players_[i]->Setup();
    }

    ball_->Setup();

    for (int i = 0; i < kWallSize; i++) {
        walls_[i]->Setup();
    }

    for (int i = 0; i < kTriggerSize; i++) {
        triggers_[i]->Setup();
    }
}

void Application::Update() {
    BaseApplication::Update();

    UpdateAllGameObjects();
}

void Application::UpdateAllGameObjects() {
    for (int i = 0; i < kPlayerSize; i++) {
        players_[i]->Update();
    }

    ball_->Update();

    for (int i = 0; i < kWallSize; i++) {
        walls_[i]->Update();
    }

    for (int i = 0; i < kTriggerSize; i++) {
        triggers_[i]->Update();
    }
}

void Application::Destroy() throw() {
    DestroyAllGameObjects();

    DestroyComponents();
}

void Application::DestroyAllGameObjects() throw() {
    for (int i = 0; i < kPlayerSize; i++) {
        players_[i]->Destroy();
        delete players_[i];
    }

    ball_->Destroy();
    delete ball_;

    for (int i = 0; i < kWallSize; i++) {
        walls_[i]->Destroy();
        delete walls_[i];
    }

    for (int i = 0; i < kTriggerSize; i++) {
        triggers_[i]->Destroy();
        delete triggers_[i];
    }

    delete game_object_factory_;
    game_object_factory_ = NULL;
}

void Application::DestroyComponents() throw() {
    ComponentList::iterator it;
    for (it = components_.begin(); it != components_.end(); ++it) {
        Component* component = *it;
        delete component;
    }

    components_.clear();
}

} /* namespace pong */

