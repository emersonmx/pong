/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_APPLICATION_APPLICATION_CONTROL_HPP_
#define PONG_APPLICATION_APPLICATION_CONTROL_HPP_

namespace pong {

class ApplicationControl {
    public:
        virtual ~ApplicationControl() {}

        virtual void Restart() = 0;
};

class DefaultApplicationControl : public ApplicationControl {
    public:
        virtual void Restart() {}
};

typedef DefaultApplicationControl NoApplicationControl;

} /* namespace pong */
#endif /* PONG_APPLICATION_APPLICATION_CONTROL_HPP_ */

