/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_APPLICATION_APPLICATION_HPP_
#define PONG_APPLICATION_APPLICATION_HPP_

#include <list>

#include <pong/application/base_application.hpp>
#include <pong/object/component/component.hpp>
#include <pong/object/game_object_factory.hpp>

namespace pong {

class Application : public BaseApplication {
    public:
        typedef std::list<Component*> ComponentList;
        typedef enum {
            kPlayer1, kPlayer2, kPlayerSize
        } ControlId;
        typedef enum {
            kTriggerPlayer1, kTriggerPlayer2, kTriggerSize
        } TriggerId;

        static const int kWallSize = 2;

        Application();

        virtual ~Application() {}

    protected:
        virtual void Setup();

        virtual void SetupOgreSceneNodes();

        virtual void SetupGameObjects();

        virtual void SetupPlayers();

        virtual void SetupBall();

        virtual void SetupWalls();

        virtual void SetupTriggers();

        virtual void SetupAllGameObjects();

        virtual void Update();

        virtual void UpdateAllGameObjects();

        virtual void Destroy() throw();

        virtual void DestroyAllGameObjects() throw();

        virtual void DestroyComponents() throw();

        GameObject* players_[kPlayerSize];
        GameObject* ball_;
        GameObject* walls_[kWallSize];
        GameObject* triggers_[kTriggerSize];
        GameObjectFactory* game_object_factory_;

        ComponentList components_;
};

} /* namespace pong */
#endif /* PONG_APPLICATION_APPLICATION_HPP_ */

