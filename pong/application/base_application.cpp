/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pong/application/base_application.hpp>

#include <sstream>

#include <OgreConfigFile.h>
#include <OgreRenderWindow.h>
#include <OgreRoot.h>

#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <mxgame/exception/exception.hpp>
#include <mxgame/exception/ogre_setup_render_system_exception.hpp>
#include <mxgame/exception/ogre_setup_render_window_exception.hpp>
#include <mxgame/exception/ogre_setup_resources_exception.hpp>
#include <mxgame/exception/ogre_setup_root_exception.hpp>
#include <mxgame/graphic/ogre_bullet_debug_drawer.hpp>
#include <mxgame/system/clock.hpp>
#include <mxgame/system/system_timer.hpp>
#include <mxgame/util/util.hpp>

#include <pong/config.h>

namespace pong {

BaseApplication::BaseApplication()
        : resource_file_name_("resource.cfg"), restart_(false),
          window_width_(640), window_height_(480), timer_(NULL), clock_(NULL),
          root_(NULL), window_(NULL), scene_manager_(NULL), camera_(NULL),
          input_manager_(NULL), mouse_(NULL), keyboard_(NULL),
          broadphase_(NULL), collision_configuration_(NULL),
          dispatcher_(NULL), solver_(NULL), dynamic_world_(NULL) {}

void BaseApplication::Restart() {
    restart_ = true;
}

void BaseApplication::RestartApplication() {
    restart_ = true;
    Destroy();
    DestroyPhysics();
    DestroyInput();
    window_->removeAllViewports();
    root_->destroySceneManager(scene_manager_);

    SetupOgreScene();
    SetupInput();
    SetupPhysics();
    Setup();
}

void BaseApplication::Initialize() {
    SetupTimer();
    SetupOgre();
    SetupInput();
    SetupPhysics();
    Setup();
}

void BaseApplication::SetupTimer() {
    timer_ = new mxgame::SystemTimer();
    clock_ = new mxgame::Clock(timer_);
    clock_->set_framerate(60);
}

void BaseApplication::SetupOgre() {
    SetupOgreRoot();
    SetupOgreResources();
    SetupOgreRenderSystem();
    SetupOgreRenderWindow();
    InitializeOgreResources();
    SetupOgreScene();
}

void BaseApplication::SetupOgreRoot() {
    try {
        root_ = new Ogre::Root();
    } catch (...) {
        Finalize();
        throw mxgame::OgreSetupRootException();
    }
}

void BaseApplication::SetupOgreResources() {
    try {
        Ogre::ConfigFile config;

        config.load(resource_file_name_);

        Ogre::ConfigFile::SectionIterator section_iterator =
            config.getSectionIterator();

        Ogre::String section_name, type_name, path;
        while (section_iterator.hasMoreElements()) {
            section_name = section_iterator.peekNextKey();

            Ogre::ConfigFile::SettingsMultiMap* settings =
                section_iterator.getNext();
            Ogre::ConfigFile::SettingsMultiMap::iterator it;
            for (it = settings->begin(); it != settings->end(); ++it) {
                type_name = it->first;
                path = it->second;

                Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                    path, type_name, section_name);
            }
        }
    } catch (...) {
        Finalize();
        throw mxgame::OgreSetupResourcesException();
    }
}

void BaseApplication::SetupOgreRenderSystem() {
    Ogre::RenderSystem* render_system =
        root_->getRenderSystemByName("OpenGL Rendering Subsystem");
    if (render_system == NULL) {
        Finalize();
        throw mxgame::OgreSetupRenderSystemException();
    }

    std::ostringstream video_mode;
    video_mode << window_width_;
    video_mode << " x ";
    video_mode << window_height_;

    render_system->setConfigOption("FSAA", "0");
    render_system->setConfigOption("Fixed Pipeline Enabled", "Yes");
    render_system->setConfigOption("Full Screen", "No");
    render_system->setConfigOption("Video Mode", video_mode.str());

    root_->setRenderSystem(render_system);
}

void BaseApplication::SetupOgreRenderWindow() {
    root_->initialise(false);
    Ogre::NameValuePairList params;
    window_ = root_->createRenderWindow("Pong", window_width_, window_height_,
                                        false);
    if (window_ == NULL) {
        Finalize();
        throw mxgame::OgreSetupRenderWindowException();
    }

    Ogre::WindowEventUtilities::addWindowEventListener(window_, this);
}

void BaseApplication::InitializeOgreResources() {
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void BaseApplication::SetupOgreScene() {
    SetupOgreSceneManager();
    SetupOgreCamera();
    SetupOgreViewport();
}

void BaseApplication::SetupOgreSceneManager() {
    scene_manager_ = root_->createSceneManager(Ogre::ST_GENERIC);
}

void BaseApplication::SetupOgreCamera() {
    camera_ = scene_manager_->createCamera("camera");
    camera_->setNearClipDistance(0.1);
    camera_->setProjectionType(Ogre::PT_ORTHOGRAPHIC);
    camera_->setOrthoWindow(window_width_, window_height_);
    Ogre::SceneNode* camera_node =
        scene_manager_->getRootSceneNode()->createChildSceneNode("camera");
    camera_node->attachObject(camera_);
    camera_node->setPosition(window_width_ / 2.f, window_height_ / 2.f, 10);
}

void BaseApplication::SetupOgreViewport() {
    window_->addViewport(camera_);
}

void BaseApplication::SetupInput() {
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

    OIS::ParamList parameters;
    std::size_t window_handle = 0;

    window_->getCustomAttribute("WINDOW", &window_handle);
    parameters.insert(
        std::make_pair("WINDOW", mxgame::ToString(window_handle)));

#if defined (PLATFORM_WINDOWS)
    parameters.insert(std::make_pair(std::string("w32_mouse"),
                                     std::string("DISCL_FOREGROUND")));
    parameters.insert(std::make_pair(std::string("w32_mouse"),
                                     std::string("DISCL_NONEXCLUSIVE")));
    parameters.insert(std::make_pair(std::string("w32_keyboard"),
                                     std::string("DISCL_FOREGROUND")));
    parameters.insert(std::make_pair(std::string("w32_keyboard"),
                                     std::string("DISCL_NONEXCLUSIVE")));
#elif defined (PLATFORM_LINUX)
    parameters.insert(
        std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
    parameters.insert(
        std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
    parameters.insert(
        std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
    parameters.insert(
        std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
#elif defined (PLATFORM_APPLE)
// The functionality to show the mouse cursor is not yet implemented.
#endif /* PLATFORM_WINDOWS */

    input_manager_ = OIS::InputManager::createInputSystem(parameters);
    mouse_ = static_cast<OIS::Mouse*>(
        input_manager_->createInputObject(OIS::OISMouse, true));
    keyboard_ = static_cast<OIS::Keyboard*>(
        input_manager_->createInputObject(OIS::OISKeyboard, true));
}

void BaseApplication::SetupPhysics() {
    broadphase_ = new btDbvtBroadphase();
    collision_configuration_ = new btDefaultCollisionConfiguration();
    dispatcher_ = new btCollisionDispatcher(collision_configuration_);
    solver_ = new btSequentialImpulseConstraintSolver();

    dynamic_world_ = new btDiscreteDynamicsWorld(dispatcher_, broadphase_,
        solver_, collision_configuration_);

    ghost_pair_callback_ = new btGhostPairCallback();
    dynamic_world_->getBroadphase()->getOverlappingPairCache()->
        setInternalGhostPairCallback(ghost_pair_callback_);

    simulation_.Setup(dynamic_world_);

    dynamic_world_->setGravity(btVector3(0, 0, 0));

#ifdef DEBUG
    debug_drawer_ = new mxgame::OgreBulletDebugDrawer(scene_manager_);
    dynamic_world_->setDebugDrawer(debug_drawer_);
#endif /* DEBUG */
}

void BaseApplication::Update() {
    if (restart_) {
        RestartApplication();
        restart_ = false;
    }

    ProcessEvents();

    dynamic_world_->stepSimulation(clock_->time() / 1000.f, 10, 1 / 256.f);

    Render();

    clock_->tick();
}

void BaseApplication::ProcessEvents() {
    Ogre::WindowEventUtilities::messagePump();
    mouse_->capture();
    keyboard_->capture();

#ifdef DEBUG
    if (keyboard_->isKeyDown(OIS::KC_T)) {
        Restart();
    } else if (keyboard_->isKeyDown(OIS::KC_Y)) {
        Exit();
    }
#endif /* DEBUG */
}

void BaseApplication::Render() {
#ifdef DEBUG
    dynamic_world_->debugDrawWorld();
#endif /* DEBUG */

    root_->renderOneFrame();
}

void BaseApplication::Finalize() throw() {
    Destroy();
    DestroyPhysics();
    DestroyInput();
    DestroyOgre();
    DestroyTimer();
}

void BaseApplication::DestroyPhysics() throw() {
#ifdef DEBUG
    delete debug_drawer_;
#endif /* DEBUG */

    simulation_.Clear();

    for (int i = dynamic_world_->getNumCollisionObjects() - 1; i >= 0; i--) {
        btCollisionObject* object =
            dynamic_world_->getCollisionObjectArray()[i];
        btRigidBody* rigid_body = btRigidBody::upcast(object);
        if (rigid_body && rigid_body->getMotionState()) {
            while (rigid_body->getNumConstraintRefs()) {
                btTypedConstraint* constraint = rigid_body->getConstraintRef(0);
                dynamic_world_->removeConstraint(constraint);
                delete constraint;
            }
            dynamic_world_->removeRigidBody(rigid_body);
        } else {
            dynamic_world_->removeCollisionObject(object);
        }

        delete object->getCollisionShape();
        delete object;
    }

    delete dynamic_world_;
    delete solver_;
    delete dispatcher_;
    delete collision_configuration_;
    delete broadphase_;
    delete ghost_pair_callback_;

    dynamic_world_ = NULL;
    solver_ = NULL;
    dispatcher_ = NULL;
    collision_configuration_ = NULL;
    broadphase_ = NULL;
    ghost_pair_callback_ = NULL;
}

void BaseApplication::DestroyInput() throw() {
    input_manager_->destroyInputObject(mouse_);
    input_manager_->destroyInputObject(keyboard_);
    OIS::InputManager::destroyInputSystem(input_manager_);
    mouse_ = NULL;
    keyboard_ = NULL;
    input_manager_ = NULL;
}

void BaseApplication::DestroyOgre() throw() {
    Ogre::WindowEventUtilities::removeWindowEventListener(window_, this);

    delete root_;
    root_ = NULL;
}

void BaseApplication::DestroyTimer() throw() {
    delete clock_;
    delete timer_;

    clock_ = NULL;
    timer_ = NULL;
}

void BaseApplication::windowResized(Ogre::RenderWindow* render_window) {
    const OIS::MouseState& mouse_state = mouse_->getMouseState();
    mouse_state.width = render_window->getWidth();
    mouse_state.height = render_window->getHeight();
}

void BaseApplication::windowClosed(Ogre::RenderWindow* render_window) {
    Exit();
}

} /* namespace pong */

