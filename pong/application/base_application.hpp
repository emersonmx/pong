/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_APPLICATION_BASE_APPLICATION_HPP_
#define PONG_APPLICATION_BASE_APPLICATION_HPP_

#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <btBulletDynamicsCommon.h>

#include <OgreWindowEventUtilities.h>

#include <mxgame/application/application.hpp>
#include <mxgame/physics/bullet_simulation.hpp>

#include <pong/application/application_control.hpp>

#ifdef DEBUG
class btIDebugDraw;
#endif /* DEBUG */

namespace Ogre {

class RenderWindow;
class Root;
class SceneManager;

} /* namespace Ogre */

namespace OIS {

class InputManager;
class Keyboard;
class Mouse;

} /* namespace OIS */

namespace mxgame {

class Clock;
class Timer;

} /* namespace mxgame */

namespace pong {

class BaseApplication
        : public mxgame::Application, public Ogre::WindowEventListener,
          public ApplicationControl {

    public:
        BaseApplication();

        virtual ~BaseApplication() {}

        virtual void Restart();

    protected:
        virtual void RestartApplication();

        virtual void Initialize();

        virtual void SetupTimer();

        virtual void SetupOgre();

        virtual void SetupOgreRoot();

        virtual void SetupOgreResources();

        virtual void SetupOgreRenderSystem();

        virtual void SetupOgreRenderWindow();

        virtual void InitializeOgreResources();

        virtual void SetupOgreScene();

        virtual void SetupOgreSceneManager();

        virtual void SetupOgreCamera();

        virtual void SetupOgreViewport();

        virtual void SetupInput();

        virtual void SetupPhysics();

        virtual void Setup() = 0;

        virtual void Update();

        virtual void ProcessEvents();

        virtual void Render();

        virtual void Finalize() throw();

        virtual void Destroy() throw() = 0;

        virtual void DestroyPhysics() throw();

        virtual void DestroyInput() throw();

        virtual void DestroyOgre() throw();

        virtual void DestroyTimer() throw();

        virtual void windowResized(Ogre::RenderWindow* render_window);

        virtual void windowClosed(Ogre::RenderWindow* render_window);

        Ogre::String resource_file_name_;

        bool restart_;

        unsigned window_width_;
        unsigned window_height_;

        mxgame::Timer* timer_;
        mxgame::Clock* clock_;
        mxgame::BulletSimulation simulation_;

        Ogre::Root* root_;
        Ogre::RenderWindow* window_;
        Ogre::SceneManager* scene_manager_;
        Ogre::Camera* camera_;

        OIS::InputManager* input_manager_;
        OIS::Mouse* mouse_;
        OIS::Keyboard* keyboard_;

        btBroadphaseInterface* broadphase_;
        btDefaultCollisionConfiguration* collision_configuration_;
        btCollisionDispatcher* dispatcher_;
        btSequentialImpulseConstraintSolver* solver_;
        btDiscreteDynamicsWorld* dynamic_world_;
        btGhostPairCallback* ghost_pair_callback_;

#ifdef DEBUG
        btIDebugDraw* debug_drawer_;
#endif /* DEBUG */
};

} /* namespace pong */
#endif /* PONG_APPLICATION_BASE_APPLICATION_HPP_ */

