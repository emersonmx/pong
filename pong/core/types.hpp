/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PONG_CORE_TYPES_HPP_
#define PONG_CORE_TYPES_HPP_

#include <mxgame/core/types.hpp>

namespace pong {

typedef struct {
    mxgame::Real x;
    mxgame::Real y;
    mxgame::Real z;
} Vector3;

typedef struct {
    mxgame::Real w;
    mxgame::Real x;
    mxgame::Real y;
    mxgame::Real z;
} Quaternion;

typedef Vector3 Position;

typedef Quaternion Rotation;

typedef Vector3 Size;

typedef Vector3 Force;

typedef Vector3 Torque;

typedef Vector3 LinearVelocity;

typedef Vector3 AngularVelocity;

typedef struct {
    mxgame::Real minimum;
    mxgame::Real maximum;
} ClampValue;

typedef struct {
    ClampValue linear_velocity;
    ClampValue angular_velocity;
} Clamp;

typedef enum {
    kNone,
    kPosition = 1,
    kRotation = 2,
    kSize = 4,
    kForce = 8,
    kTorque = 16,
    kLinearVelocity = 32,
    kAngularVelocity = 64,
    kClampVelocity = 128,
    kAllMessages = 255
} Message;

} /* namespace pong */
#endif /* PONG_CORE_TYPES_HPP_ */

