/*
  Copyright (C) 2013 Emerson Max de Medeiros Silva

  This file is part of pong.

  pong is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  pong is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pong.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <mxgame/exception/exception.hpp>

#include <pong/application/application.hpp>

int main() {
    using namespace std;

    mxgame::Application* application = new pong::Application();

    try {
        application->Run();
    } catch(const mxgame::Exception& exception) {
        cout << exception.what();
        application->Exit(EXIT_FAILURE);
    } catch(const std::exception& exception) {
        cout << exception.what();
        application->Exit(EXIT_FAILURE);
    } catch(...) {
        cout << "Unknown error.\n";
        application->Exit(EXIT_FAILURE);
    }

    int error_code = application->error_code();

    delete application;

#ifdef DEBUG
    std::string error_name = "No Error";

    if (error_code) {
        error_name = "Error";
    }

    cout << "Error code: " << error_code
         << " (" << error_name << ")\n";
#endif /* DEBUG */

    return error_code;
}

