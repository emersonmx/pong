pong
====

Um jogo de pong em OpenGL, Ogre3D, SDL, ou qualquer outra biblioteca gráfica que
eu quiser testar :D

Dependências
------------

* `Boost <http://www.boost.org/>`_ >= 1.53.0

* `Ogre <http://www.ogre3d.org/>`_ >= 1.8.1

* `Bullet <http://www.bulletphysics.org/>`_ >= 1.80

Instalação normal
-----------------

.. sourcecode:: bash

 $ ./autogen.sh
 $ ./configure --prefix=/<caminho_instalacao>/pong
 $ make
 $ make install
 $ cp -r dist/media/ /<caminho_instalacao>/pong/

Instalação da versão para desenvolvimento
-----------------------------------------

.. sourcecode:: bash

 $ ./autogen.sh
 $ ./configure --prefix=$(pwd)/dist CXXFLAGS="-g -O0 -pipe" CPPFLAGS="-DDEBUG"
 $ make install
 $ cd dist/media/source/
 $ make install

Execução
--------

.. sourcecode:: bash

    cd dist/
    ./pong


Teclas
------

::

    Jogador 1:
        Up -> UP
        Down -> DOWN
        Left -> LEFT
        Right -> RIGHT
        Enter -> RETURN
        Back -> ESCAPE
        Pause -> P
        Select -> SPACE

    Jogador 2:
        Up -> W
        Down -> S
        Left -> A
        Right -> D
        Enter -> E
        Back -> Q
        Pause -> L
        Select -> F
